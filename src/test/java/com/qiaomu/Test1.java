package com.qiaomu;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.img.ImgUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.URLUtil;
import cn.hutool.crypto.digest.DigestUtil;
import cn.hutool.crypto.digest.MD5;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.qiaomu.interfaces.Operate;
import com.qiaomu.interfaces.impl.OperateImpl;
import com.qiaomu.model.BatchInfo;
import com.qiaomu.model.Result;
import com.qiaomu.model.SeedResult;
import com.qiaomu.model.ValidatedCode;
import com.qiaomu.tools.UKeyHelper;
import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.HttpCookie;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static cn.hutool.core.img.ImgUtil.IMAGE_TYPE_JPEG;
import static com.qiaomu.tools.FileUtil.creatParentFileAndPermission;

/**
 * @author: qiaomu
 * @date: 2021/7/7 17:49
 * @Description: TODO
 */
public class Test1 {

    private static final String domain = "https://xj.122.gov.cn";

    @Test
    public void function() throws IOException {
//        String url = "https://127.0.0.1:53015/ia300";
//        HashMap<String, Object> map = new HashMap<>();
//        map.put("function", "IA300CheckExist");

//        String content = "json={\"function\":\"IA300CheckExist\"}";
//        String content = "json={\"function\":\"IA300Open\", \"passWd\":\"24680!#%&(tmri\"}";
//        String content = "json={\"function\":\"IA300GetUID\"}";
//        String result = HttpUtil.post(url, content);
//        System.out.println(result);

        Result<String> uKeyIdResult = UKeyHelper.getUKeyId();
        String uKeyId = uKeyIdResult.getData();
        System.out.println(uKeyId);



        String url =  domain + "/m/login?t=2";
        List<HttpCookie> httpCookies = HttpUtil.createGet(url).execute().getCookies();
//        HttpCookie[] httpCookies = new HttpCookie[cookies.size()];
//        for (int i = 0; i < cookies.size(); i++) {
//            httpCookies[i] = cookies.get(i);
//        }
        System.out.println("========================================");
        String url2 =  domain + "/m/tmri/captcha/mcheckType?checktype=sgcFtVUtwJZtzwTT";
        String body = HttpRequest.post(url2)
                .header("Cookie", JSON.toJSONString(httpCookies))
                .execute().body();
        System.out.println(body);


        //获取验证码
        InputStream inputStream = HttpRequest.get("https://xj.122.gov.cn/m/tmri/captcha/math?nocache=" + System.currentTimeMillis())
                .header("Cookie", JSON.toJSONString(httpCookies))
                .execute().bodyStream();
        BufferedImage bufferedImage = ImageIO.read(inputStream);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ImgUtil.write(bufferedImage, IMAGE_TYPE_JPEG, out);
        byte[] bytes = out.toByteArray();
        String base64Str = Base64.encode(bytes);
        String dataUriBase64 = URLEncoder.encode(base64Str, "utf-8");
        String contentJson = "webSide=https://d.122.gov.cn&b64=" + dataUriBase64 + "&sign=htbb";
        //识别验证码
        String verCode = HttpUtil.post("http://106.75.148.97:19996/ocr", contentJson);
        ValidatedCode validatedCode = JSON.parseObject(verCode, ValidatedCode.class);
        System.out.println(verCode);

        //验证验证码
        Map<String, Object> map = new HashMap<>();
        map.put("usertype", "2");
        map.put("systemid", "main");
        map.put("szzsid", uKeyId);
        map.put("username", "65012009083842");
        map.put("password", Base64.encode("ahTU8wxQb"));
        map.put("csessionid", validatedCode.getResult());
        map.put("C2D6C3D4869A1D7907E3D321CF28917A", "97d5bcb3f44ef458fa70a5cc7fa7c47a");
        String result = HttpRequest.post(domain + "/user/m/login/checkkey")
                .header("Cookie", JSON.toJSONString(httpCookies))
                .form(map)
                .execute().body();
        System.out.println(result);

        JSONObject jsonObject = JSON.parseObject(result);
        String message = UKeyHelper.IA300SHA1WithSeed(jsonObject.getString("message"));
//        System.out.println(message);
        SeedResult seedResult = JSON.parseObject(message, SeedResult.class);
        System.out.println(seedResult.getDigest());

        Map<String, Object> loginMap = new HashMap<>();
        loginMap.put("usertype", "2");
        loginMap.put("systemid", "main");
        loginMap.put("ticket", seedResult.getDigest());
        loginMap.put("szzsid", uKeyId);
        loginMap.put("username", "650120090838421");
        loginMap.put("password", Base64.encode("ahTU8wxQb"));
        loginMap.put("csessionid", validatedCode.getResult());
        loginMap.put("C2D6C3D4869A1D7907E3D321CF28917A", "97d5bcb3f44ef458fa70a5cc7fa7c47a");

        System.out.println("========================================================");
        System.out.println(JSON.toJSONString(loginMap));
        HttpResponse execute = HttpRequest.post(domain + "/user/m/loginkey")
                .header("Cookie", JSON.toJSONString(httpCookies))
                .form(loginMap)
                .execute();
        Map<String, List<String>> headers = execute.headers();
        List<String> list = headers.get("Location");
        for (String str : list) {
            System.out.println(str);
        }
        HttpResponse loginSuccess = HttpRequest.get(list.get(0))
                .header("Cookie", JSON.toJSONString(httpCookies))
                .execute();
//        Map<String, List<String>> loginSuccessHeaders = loginSuccess.headers();
//        List<String> loginSuccessLocations = loginSuccessHeaders.get("Location");
//        HttpResponse bbb = HttpRequest.get(loginSuccessLocations.get(0))
//                .header("Cookie", JSON.toJSONString(httpCookies))
//                .execute();
//        System.out.println("========================================================");
//        System.out.println(bbb.body());


        String userInfo = HttpRequest.post(domain + "/user/m/userinfo/basic")
                .header("Cookie", JSON.toJSONString(httpCookies))
                .execute().body();
        System.out.println(userInfo);

    }

    @Test
    public void func2() throws IOException {
        Operate operate = new OperateImpl();
        operate.checkLogin();
    }

    @Test
    public void func3() throws IOException {
        Operate operate = new OperateImpl();
        operate.checkLogin();
//        operate.pullData();
    }

    @Test
    public void func4() {
        String now = DateUtil.now();
        System.out.println(now);
        System.out.println(DateUtil.today());
        DateTime dateTime = DateUtil.offsetDay(DateUtil.date(), -30);
        System.out.println(dateTime);
    }

    @Test
    public void func5() {
        String property = System.getProperty("user.dir");
        System.out.println(property);//user.dir指定了当前的路径
        File file = new File(property);
        String parent = file.getParent();
        System.out.println(parent);
        File file2 = new File(file.getParent(), DateUtil.today());
        if (!file2.exists()) {
            file2.mkdir();
            file2.setWritable(true);
            file2.setReadable(true);
        }

        System.out.println(file2.getAbsolutePath()  + File.separator + "aaa.txt");

    }

    @Test
    public void func6() throws IOException {
        File file = new File("C:\\aaa\\driverTest", DateUtil.today());
        creatParentFileAndPermission(file);
        java.awt.Desktop.getDesktop().open(file);
    }

    @Test
    public void func7() throws InterruptedException {
        System.out.println(DateUtil.now());
        Thread mainThread = Thread.currentThread();  //主线程
        ScheduledExecutorService scheduledThreadPool = Executors.newScheduledThreadPool(5);
        scheduledThreadPool.scheduleAtFixedRate(() -> System.out.println(DateUtil.now()), 1, 5, TimeUnit.SECONDS);
        if (Thread.activeCount() > 2) {
            mainThread.join();
        }
        System.out.println("====================");
    }

    @Test
    public void func8() throws UnsupportedEncodingException {
        String account = "{\"account\":\"65012009083842\",\"authKey\":\"fc145eb157ddec5af525e0ea76c2c5fc\",\"organName\":\"驾校B\",\"passWord\":\"ahTU8wxQb\",\"registerOrgan\":\"驾校B\",\"registerUser\":\"*辉\",\"reportName\":\"乌鲁木齐金运通机动车驾驶培训学校（有限公司)\",\"uKeyId\":\"dc59550653ba91642dbda7bc54d64ca5\",\"updateTime\":\"2021-07-27 14:35:50\",\"webSite\":\"https://xj.122.gov.cn\"}";

        BatchInfo info = new BatchInfo();
        info.setGetTime(DateUtil.now());
        info.setInfoType("约考");
        info.setRowsCount(6);
        info.set_abstract("2021-07-27_C2_科目2_约考");

        String url = "https://testinterface.haitunbx.com/htbx-interface/udun122/upload";
        String result = HttpRequest.post(url)
                .header("account", URLEncoder.encode(account, "utf-8"))
                .header("batchInfo", URLEncoder.encode(JSON.toJSONString(info), "utf-8"))
                .header("content-type", "utf-8")
                .form("file", new File("C:\\driver\\2021-07-27\\2021-07-27_科目2_约考_C2.xls"))
                .execute().body();

        System.out.println(result);
    }

    @Test
    public void func9() {
        String key = "hthu.com.cn"; //密钥Key
        String account = "65012009083842";
        String organName = "驾校B";
        String authKey = "fc145eb157ddec5af525e0ea76c2c5fc";

        String content = key + "|" + account + "|" + organName;
        System.out.println("content ======== " + content);

        String md5 = DigestUtil.md5Hex(content, CharsetUtil.GBK);
        System.out.println(md5);
        System.out.println(authKey);
    }
}
