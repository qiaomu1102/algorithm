package com.qiaomu;


import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.StreamProgress;
import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.qiaomu.common.BizException;
import com.qiaomu.interfaces.Operate;
import com.qiaomu.interfaces.SyncDataCompleteListener;
import com.qiaomu.interfaces.impl.OperateImpl;
import com.qiaomu.model.AccountInfo;
import com.qiaomu.model.Const;
import com.qiaomu.model.UrlConst;
import com.qiaomu.tools.FileUtil;
import com.qiaomu.tools.StringUtil;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.qiaomu.tools.FileUtil.creatParentFileAndPermission;

/**
 * @author: qiaomu
 * @date: 2021/7/6 12:22
 * @Description: TODO
 */

public class DriverApplication {

    static final int marginLeft = 20;
    static final int textLocationX = 100;

    static JFrame frame;
    static JTextField webSideText;
    static JTextField accountText;
    static JPasswordField passwordText;
    static JTextField driveText;
    static JTextField authKeyText;
    static JLabel operateText;
    static JLabel synDateTime;
    static JButton operateBtn;
    static JButton scanBtn;
    static JButton fileButton;
    public static Operate operate;

    public static void main(String[] args) {
        initUI();
        initData();
        operate = new OperateImpl();

        operate();
        executeTask();
    }

    /**
     * //每15分钟保持一次登陆状态
     * // 每 60分钟做一次定时同步
     */
    private static void executeTask() {
        ScheduledExecutorService scheduledThreadPool = Executors.newScheduledThreadPool(2);
//        scheduledThreadPool.scheduleAtFixedRate(() -> {
//            try {
//                System.out.println("\n定时任务登陆=====================" + DateUtil.now() + "\n");
//                operateText.setText("正在登陆...");
//                Const.isLogin = false;
//                operate.checkLogin();
//            } catch (BizException e) {
//                e.printStackTrace();
//                operateText.setText(e.getMessage());
//            }
//        }, 2, 5, TimeUnit.MINUTES);

        scheduledThreadPool.scheduleAtFixedRate(() -> {
            try {
                System.out.println("\n定时任务拉取数据=====================" + DateUtil.now() + "\n");
                operateText.setText("拉取数据...");
                operate.pullData(() -> {
                    operateText.setText("同步数据完成");
                    synDateTime.setText("最近同步时间：" + DateUtil.now());
                    Const.accountInfo.setUpdateTime(DateUtil.now());
                    FileUtil.saveCache(JSON.toJSONString(Const.accountInfo));
                });

            } catch (Exception e) {
                e.printStackTrace();
                operateText.setText(e.getMessage());
            }
        }, 60, 60, TimeUnit.MINUTES);
    }

    private static void operate() {
        //同步数据
        operateBtn.addActionListener(e -> {
            String msg = checkAccount();
            if (StringUtil.isNotEmpty(msg)) {
                JOptionPane.showMessageDialog(frame, msg);
                return;
            }
            ThreadUtil.execute(() -> {
                try {
                    operateText.setText("正在登陆...");
                    boolean loginFlag = operate.checkLogin();
                    if (loginFlag) {
                        operateText.setText("拉取数据...");
                        operate.pullData(() -> {
                            operateText.setText("同步数据完成");
                            synDateTime.setText("最近同步时间：" + DateUtil.now());
                            Const.accountInfo.setUpdateTime(DateUtil.now());
                            FileUtil.saveCache(JSON.toJSONString(Const.accountInfo));
                        });
                    }

                } catch (BizException | IOException ex) {
                    ex.printStackTrace();
                    operateText.setText(ex.getMessage());
                }

            });
        });

        //浏览数据目录
        scanBtn.addActionListener(e -> {
            try {

                File file = null;
                if (StringUtil.isEmpty(Const.filePath)) {
                    file = FileUtil.createDir();
                } else {
                    file = new File(Const.filePath);
                }

                Desktop.getDesktop().open(file);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });

        //文件导入
        fileButton.addActionListener(e -> {
            FileDialog openFile = new FileDialog(frame, "选择文件", FileDialog.LOAD);
            openFile.setVisible(true);
            String dirName = openFile.getDirectory();
            String fileName = openFile.getFile();

            //读取展示文件
            if (dirName == null || fileName == null) {
                return;
            }
            File file = new File(dirName, fileName);
            try {
                BufferedReader br = new BufferedReader(new FileReader(file));
                String line;
                StringBuilder text = new StringBuilder();
                while ((line = br.readLine()) != null) {
                    text.append(line);
                    text.append("\r\n");
                }

                String configStr = text.toString();
                Const.accountInfo = JSON.parseObject(configStr, AccountInfo.class);
                Const.accountInfo.setRegisterOrgan(Const.accountInfo.getOrganName());
                FileUtil.saveCache(configStr);

                webSideText.setText(Const.accountInfo.getWebSite());
                accountText.setText(Const.accountInfo.getAccount());
                passwordText.setText(Const.accountInfo.getPassWord());
                driveText.setText(Const.accountInfo.getOrganName());
                authKeyText.setText(Const.accountInfo.getAuthKey());
            } catch (Exception e1) {
                JOptionPane.showMessageDialog(frame, e1.getMessage());
            }

        });

    }

    private static void initUI() {
        String config = FileUtil.readCache();
        System.out.println("accountInfo ======== " + config);
        if (StringUtil.isNotEmpty(config)) {
            Const.accountInfo = JSON.parseObject(config, AccountInfo.class);
        }

        URL resource = DriverApplication.class.getClassLoader().getResource("images/logo.jpg");
        ImageIcon imageIcon = new ImageIcon(resource);
        // 创建 JFrame 实例
        frame = createFrame(imageIcon);
        JPanel panel = new JPanel();
        LineBorder lb = new LineBorder(Color.lightGray, 1, false);

        JPanel configPanel = createConfigPanel();
        configPanel.setPreferredSize(new Dimension(400, 270));
        configPanel.setBorder(BorderFactory.createTitledBorder(lb, "配置"));

        JPanel operatePanel = createOperatePanel();
        operatePanel.setPreferredSize(new Dimension(400, 100));
        operatePanel.setBorder(BorderFactory.createTitledBorder(lb, "状态"));

        JPanel versionPanel = createVersionPanel();
        versionPanel.setPreferredSize(new Dimension(400, 60));
        versionPanel.setBorder(BorderFactory.createTitledBorder(lb, "版本"));

        panel.add(configPanel);
        panel.add(operatePanel);
        panel.add(versionPanel);

        frame.setContentPane(panel);

        // 设置界面可见
        frame.setVisible(true);
    }

    private static void initData() {
        if (Const.accountInfo != null) {
            webSideText.setText(Const.accountInfo.getWebSite());
            accountText.setText(Const.accountInfo.getAccount());
            passwordText.setText(Const.accountInfo.getPassWord());
            driveText.setText(Const.accountInfo.getOrganName());
            authKeyText.setText(Const.accountInfo.getAuthKey());
            synDateTime.setText("最近同步时间：" + Const.accountInfo.getUpdateTime());
        }
    }

    private static JPanel createVersionPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(null);

        JLabel operateText = new JLabel("学员信息同步助手 V 1.0");
        operateText.setBounds(marginLeft, 20, 200, 25);
        panel.add(operateText);

        JButton updateBtn = new JButton("更新");
        updateBtn.setBounds(240, 20, 80, 25);
        panel.add(updateBtn);
        updateBtn.addActionListener(e -> {
            File file = new File(Const.saveFilePath);
            creatParentFileAndPermission(file);
            HttpUtil.downloadFile(UrlConst.downLoadUrl, file);
        });

        return panel;
    }

    private static JPanel createOperatePanel() {
        JPanel panel = new JPanel();
        panel.setLayout(null);

        operateText = new JLabel("");
        operateText.setBounds(marginLeft, 30, 240, 25);
        panel.add(operateText);

        operateBtn = new JButton("手工同步测试");
        operateBtn.setBounds(260, 30, 120, 25);
        panel.add(operateBtn);

        synDateTime = new JLabel("最近同步时间：");
        synDateTime.setBounds(marginLeft, 60, 240, 25);
        panel.add(synDateTime);

        scanBtn = new JButton("浏览同步目录");
        scanBtn.setBounds(260, 60, 120, 25);
        panel.add(scanBtn);

        return panel;
    }

    private static String checkAccount() {
        if (StringUtil.isEmpty(webSideText.getText())) {
            return "请输入省网网址";
        }

        if (StringUtil.isEmpty(accountText.getText())) {
            return "请输入登陆账号";
        }

        if (StringUtil.isEmpty(new String(passwordText.getPassword()))) {
            return "请输入登陆密码";
        }

        if (StringUtil.isEmpty(driveText.getText())) {
            return "请输入驾校名称";
        }

        if (StringUtil.isEmpty(authKeyText.getText())) {
            return "请输入AuthKey";
        }

        return null;
    }

    private static JPanel createConfigPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(null);
        // 创建 JLabel
        JLabel webSide = new JLabel("省网网址:");
        webSide.setBounds(marginLeft, 25, 80, 25);
        panel.add(webSide);
        webSideText = new JTextField(20);
        webSideText.setBounds(textLocationX, 25, 250, 25);
        panel.add(webSideText);

        JLabel userLabel = new JLabel("登陆账号:");
        userLabel.setBounds(marginLeft, 65, 80, 25);
        panel.add(userLabel);
        accountText = new JTextField(20);
        accountText.setEnabled(false);
        accountText.setBounds(textLocationX, 65, 250, 25);
        panel.add(accountText);

        JLabel passwordLabel = new JLabel("登陆密码:");
        passwordLabel.setBounds(marginLeft, 105, 80, 25);
        panel.add(passwordLabel);
        passwordText = new JPasswordField(20);
        passwordText.setBounds(textLocationX, 105, 250, 25);
        panel.add(passwordText);

        JLabel driveLabel = new JLabel("驾校名称:");
        driveLabel.setBounds(marginLeft, 145, 80, 25);
        panel.add(driveLabel);
        driveText = new JTextField(20);
        driveText.setEnabled(false);
        driveText.setBounds(textLocationX, 145, 250, 25);
        panel.add(driveText);

        JLabel authKeyLabel = new JLabel("AuthKey:");
        authKeyLabel.setBounds(marginLeft, 185, 80, 25);
        panel.add(authKeyLabel);
        authKeyText = new JTextField(20);
        authKeyText.setBounds(textLocationX, 185, 250, 25);
        authKeyText.setEnabled(false);
        panel.add(authKeyText);

        // 文件选择按钮
        fileButton = new JButton("从文件导入配置");
        fileButton.setBounds(100, 220, 200, 25);
        panel.add(fileButton);


        return panel;
    }

    private static JFrame createFrame(ImageIcon imageIcon) {
        JFrame frame = new JFrame("状态&配置");
        frame.setSize(450, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Toolkit kit = Toolkit.getDefaultToolkit();
        Dimension screenSize = kit.getScreenSize();
        int screenWidth = screenSize.width;
        int screenHeight = screenSize.height;
        frame.setLocation(screenWidth * 2 / 5, screenHeight / 4);
        frame.setIconImage(imageIcon.getImage());//设置LOGO
        return frame;
    }

}
