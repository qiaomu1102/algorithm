package com.qiaomu.common;

/**
 * @author: qiaomu
 * @date: 2021/7/20 17:06
 * @Description: TODO
 */
public class BizException extends RuntimeException {

    public BizException() {
    }

    public BizException(String message) {
        super(message);
    }
}
