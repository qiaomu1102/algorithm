package com.qiaomu.tools;

import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.qiaomu.model.Result;
import com.qiaomu.model.UDunExist;
import com.qiaomu.model.UDunID;
import com.qiaomu.model.UDunOpen;

/**
 * @author: qiaomu
 * @date: 2021/7/15 10:25
 * @Description: TODO
 */
public class UKeyHelper {

    public static Result<String> getUKeyId() {
        String url = "https://127.0.0.1:53015/ia300";
        try {
            String existStr = "json={\"function\":\"IA300CheckExist\"}";
            String existResult = HttpUtil.post(url, existStr);
            UDunExist uDunExist = JSON.parseObject(existResult, UDunExist.class);
            if (uDunExist.getDevCount() != 1) {
                return Result.fail("U盾不存在");
            }

            String openStr = "json={\"function\":\"IA300Open\", \"passWd\":\"24680!#%&(tmri\"}";
            String openResult = HttpUtil.post(url, openStr);
            UDunOpen uDunOpen = JSON.parseObject(openResult, UDunOpen.class);
            if (uDunOpen.getErrorCode() != 0) {
                return Result.fail("打开U盾失败");
            }

            String uIdStr = "json={\"function\":\"IA300GetUID\"}";
            String uIdResult = HttpUtil.post(url, uIdStr);
            UDunID uDunID = JSON.parseObject(uIdResult, UDunID.class);
            String hardwareID = uDunID.getHardwareID();
            if (hardwareID == null || hardwareID.trim().length() == 0) {
                return Result.fail("获取U盾信息失败");
            }
            System.out.println("uKeyId ========== " + hardwareID);
            return Result.success(hardwareID);
        } catch (RuntimeException e) {
            e.printStackTrace();
            return Result.fail("请下载U盾驱动!");
        }
    }

    public static String IA300SHA1WithSeed(String seed){
        String url = "https://127.0.0.1:53015/ia300";
        String content = "json={\"function\":\"IA300SHA1WithSeed\", \"Seed\":\"" + seed + "\"}";
        return HttpUtil.post(url, content);
    }
}
