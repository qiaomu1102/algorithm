package com.qiaomu.tools;

/**
 * @author: qiaomu
 * @date: 2021/7/20 17:14
 * @Description: TODO
 */
public class StringUtil {

    public static boolean isEmpty(String content) {
        return content == null || content.trim().length() == 0;
    }

    public static boolean isNotEmpty(String content) {
        return content != null && content.trim().length() > 0;
    }
}
