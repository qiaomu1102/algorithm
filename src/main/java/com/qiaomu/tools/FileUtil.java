package com.qiaomu.tools;

import cn.hutool.core.date.DateUtil;
import com.qiaomu.model.Const;

import java.io.*;

/**
 * @author: qiaomu
 * @date: 2021/7/20 10:16
 * @Description: TODO
 */
public class FileUtil {

    public static String saveFile(InputStream inputStream, String fileName) {
        File filePath = createDir();
        Const.filePath = filePath.getAbsolutePath();

        BufferedInputStream in = null;
        BufferedOutputStream out = null;
        try {
            in = new BufferedInputStream(inputStream);
            String file = filePath + File.separator + fileName;
            out = new BufferedOutputStream(new FileOutputStream(file));
            int len = -1;
            byte[] b = new byte[1024];
            while ((len = in.read(b)) != -1) {
                out.write(b, 0, len);
            }
            out.flush();
            return file;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (in != null) {
                    in.close();
                }

                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 创建目录设置权限
     * @param file
     */
    public static void creatParentFileAndPermission(File file) {
        File parentFile = file.getParentFile();
        if (!parentFile.exists()) {
            creatParentFileAndPermission(parentFile);
        }

        if (!file.exists()) {
            boolean mkdirResult = file.mkdir();
            System.out.println("创建目录结果： " + mkdirResult);
            boolean b = file.setExecutable(true, false);//设置可执行权限
            boolean b1 = file.setReadable(true, false);//设置可读权限
            boolean b2 = file.setWritable(true);
        }
    }

    public static File createDir() {
        File file = new File(Const.saveFilePath, DateUtil.today());
        creatParentFileAndPermission(file);
        return file;
    }

    public static void saveCache(String configStr) {
        FileWriter fileWriter = null;
        try {
            File file = new File(Const.localPath, "driveConfig.json");
            fileWriter = new FileWriter(file);
            fileWriter.write(configStr);
            fileWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileWriter != null) {
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public static String readCache() {
        FileReader fileReader = null;
        BufferedReader reader = null;
        try {
            File file = new File(Const.localPath, "driveConfig.json");
            System.out.println(file.getAbsolutePath());
            fileReader = new FileReader(file);
            reader = new BufferedReader(fileReader);
            String str = "";
            String line;
            while ((line = reader.readLine()) != null){
                str += line;
            }
            return str;
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (fileReader != null) {
                    fileReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
