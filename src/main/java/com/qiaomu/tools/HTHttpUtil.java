package com.qiaomu.tools;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.img.ImgUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import cn.hutool.http.Method;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.qiaomu.DriverApplication;
import com.qiaomu.common.BizException;
import com.qiaomu.interfaces.Operate;
import com.qiaomu.interfaces.impl.OperateImpl;
import com.qiaomu.model.*;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.HttpCookie;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static cn.hutool.core.img.ImgUtil.IMAGE_TYPE_JPEG;
import static cn.hutool.core.img.ImgUtil.read;

/**
 * @author: qiaomu
 * @date: 2021/7/16 15:16
 * @Description: TODO
 */
public class HTHttpUtil {

    public static void saveCookies() {
        List<HttpCookie> cookies = HttpUtil.createGet(Const.accountInfo.getWebSite() + UrlConst.loginPage).execute().getCookies();
        Const.cookies = JSON.toJSONString(cookies);
    }

    public static void checkType() {
        String body = HttpRequest.post(Const.accountInfo.getWebSite() + UrlConst.checkType)
                .header("Cookie", Const.cookies)
                .execute().body();
        System.out.println("获取网络=========" + body);
    }

    public static String captcha(){
        //获取验证码
        InputStream inputStream = HttpRequest.get(Const.accountInfo.getWebSite() + UrlConst.captcha + System.currentTimeMillis())
                .header("Cookie", Const.cookies)
                .execute().bodyStream();
        try {
            BufferedImage bufferedImage = ImageIO.read(inputStream);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ImgUtil.write(bufferedImage, IMAGE_TYPE_JPEG, out);
            byte[] bytes = out.toByteArray();
            String base64Str = Base64.encode(bytes);
            String dataUriBase64 = URLEncoder.encode(base64Str, "utf-8");
            String contentJson = UrlConst.ocr_captcha_params.replace("IMGBASE64STR", dataUriBase64);
            //识别验证码
            String verCode = HttpUtil.post(UrlConst.ocr_captcha_domain, contentJson);
            System.out.println("识别验证码========" + verCode);
            ValidatedCode validatedCode = JSON.parseObject(verCode, ValidatedCode.class);
            return validatedCode.getResult();
        } catch (IOException e) {
            throw new BizException("网络异常，请稍后重试！");
        }
    }

    public static String checkKey(String validatedCode) {
        //验证验证码
        Map<String, Object> map = new HashMap<>();
        map.put("usertype", "2");
        map.put("systemid", "main");
        map.put("szzsid", Const.accountInfo.getuKeyId());
        map.put("username", Const.accountInfo.getAccount());
        map.put("password", Base64.encode(Const.accountInfo.getPassWord()));
        map.put("csessionid", validatedCode);
        map.put("C2D6C3D4869A1D7907E3D321CF28917A", "97d5bcb3f44ef458fa70a5cc7fa7c47a");
        String result = HttpRequest.post(Const.accountInfo.getWebSite() + UrlConst.check_key)
                .header("Cookie", Const.cookies)
                .form(map)
                .execute().body();
        System.out.println("checkKey============" + result);
        return result;

    }

    public static String seed(String seed) {
        String message = UKeyHelper.IA300SHA1WithSeed(seed);
        System.out.println("Seed==========" + message);
        SeedResult seedResult = JSON.parseObject(message, SeedResult.class);
        String ticket = null;
        if (seedResult.getErrorCode() == 0) {
            ticket = seedResult.getDigest();
        }
        return ticket;
    }

    public static void login(String ticket, String validatedCode) {
        Map<String, Object> loginMap = new HashMap<>();
        loginMap.put("usertype", "2");
        loginMap.put("systemid", "main");
        loginMap.put("ticket", ticket);
        loginMap.put("szzsid", Const.accountInfo.getuKeyId());
        loginMap.put("username", Const.accountInfo.getAccount());
        loginMap.put("password", Base64.encode(Const.accountInfo.getPassWord()));
        loginMap.put("csessionid", validatedCode);
        loginMap.put("C2D6C3D4869A1D7907E3D321CF28917A", "97d5bcb3f44ef458fa70a5cc7fa7c47a");

        HttpResponse execute = HttpRequest.post(Const.accountInfo.getWebSite() + UrlConst.login_key)
                .header("Cookie", Const.cookies)
                .form(loginMap)
                .execute();
        Map<String, List<String>> headers = execute.headers();
        List<String> list = headers.get("Location");
//        for (String str : list) {
//            System.out.println(str);
//        }
        HttpRequest.get(list.get(0))
                .header("Cookie", Const.cookies)
                .execute();
    }

    public static String userInfo() {
        String userInfo = HttpRequest.post(Const.accountInfo.getWebSite() + UrlConst.userInfo)
                .header("Cookie", Const.cookies)
                .execute().body();
        System.out.println("userInfo ============ " + userInfo);
        return userInfo;
    }

    public static InputStream executeDown(Map<String, Object> params) {
        String param = "";
        for (Map.Entry<String, Object> entry : params.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            param += key + "=" + value + "&";
        }

        param = param.substring(0, param.length() - 1);
        HttpResponse response = execute(Const.accountInfo.getWebSite() + UrlConst.execute_down + "?" + param, Method.POST);
        return response.bodyStream();
    }

    public static HttpResponse execute(String url, Method method) {
        HttpResponse response = doRequest(url, method);

        int status = response.getStatus();
        if (status == 302 || status == 303 || status == 301) {
            Const.isLogin = false;
            try {
                System.out.println("重新登录===================");
                boolean isLogin = DriverApplication.operate.checkLogin();
                if (isLogin) {
                    return doRequest(url, method);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return response;
    }

    private static HttpResponse doRequest(String url, Method method) {
        return new HttpRequest(url).method(method)
                    .header("Cookie", Const.cookies)
                    .execute();
    }

    public static void uploadFile(String filePath, BatchInfo batchInfo) {
        try {
            System.out.println(JSON.toJSONString(Const.accountInfo));
            String result = HttpRequest.post(UrlConst.uploadUrl)
                    .header("account", URLEncoder.encode(JSON.toJSONString(Const.accountInfo), "utf-8"))
                    .header("batchInfo", URLEncoder.encode(JSON.toJSONString(batchInfo), "utf-8"))
                    .header("content-type", "utf-8")
                    .form("file", new File(filePath))
                    .execute().body();
            System.out.println(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
