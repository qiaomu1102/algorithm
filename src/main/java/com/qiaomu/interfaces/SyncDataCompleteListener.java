package com.qiaomu.interfaces;

/**
 * @author: qiaomu
 * @date: 2021/7/28 14:46
 * @Description: TODO
 */
public interface SyncDataCompleteListener {

    void onComplete();
}
