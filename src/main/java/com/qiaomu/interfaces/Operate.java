package com.qiaomu.interfaces;

import java.io.IOException;

/**
 * @author: qiaomu
 * @date: 2021/7/15 10:17
 * @Description: TODO
 */
public interface Operate {

    boolean checkLogin() throws IOException;

    void pullData(SyncDataCompleteListener listener);
}
