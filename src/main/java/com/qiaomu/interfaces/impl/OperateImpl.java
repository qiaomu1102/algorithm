package com.qiaomu.interfaces.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.qiaomu.common.BizException;
import com.qiaomu.interfaces.Operate;
import com.qiaomu.interfaces.SyncDataCompleteListener;
import com.qiaomu.model.BatchInfo;
import com.qiaomu.model.Const;
import com.qiaomu.model.Result;
import com.qiaomu.tools.FileUtil;
import com.qiaomu.tools.HTHttpUtil;
import com.qiaomu.tools.StringUtil;
import com.qiaomu.tools.UKeyHelper;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author: qiaomu
 * @date: 2021/7/15 10:22
 * @Description: TODO
 */
public class OperateImpl implements Operate {

    private static Map<String, String> kmMap;
    private static Map<String, String> typeMap;
    private static Map<String, String> cMap;
    private static Map<String, String> typeNameMap;
    private static ExecutorService executorService;

    static {
        kmMap = getKmMap();
        typeMap = getTypeMap();
        cMap = getCMap();
        typeNameMap = getTypeNameMap();

        executorService = Executors.newFixedThreadPool(5);
    }


    @Override
    public boolean checkLogin() throws BizException {
        if (Const.isLogin) {
            return true;
        }

        //获取U盾id
        Result<String> uKeyIdResult = UKeyHelper.getUKeyId();
        if (uKeyIdResult.getCode() != 0) {
            if (StringUtil.isEmpty(Const.accountInfo.getuKeyId())) {
                throw new BizException(uKeyIdResult.getMsg());
            }
        } else {
            //写入缓存
            Const.accountInfo.setuKeyId(uKeyIdResult.getData());

        }

        //获取cookies
        HTHttpUtil.saveCookies();
        //checkType
        HTHttpUtil.checkType();
        //获取、识别验证码
        String captcha = HTHttpUtil.captcha();
        //checkKey
        String result = HTHttpUtil.checkKey(captcha);
        JSONObject jsonObject = JSON.parseObject(result);
        String message = jsonObject.getString("message");
        if (jsonObject.getInteger("code") != 200) {
            if ("重复登陆".equals(message)) {
                Const.isLogin = true;
                return true;
            }
            throw new BizException(message);
        }

        String ticket = HTHttpUtil.seed(message);
        System.out.println("ticket===========" + ticket);
        if (StringUtil.isEmpty(ticket)) {
            throw new BizException("U盾不存在");
        }
        HTHttpUtil.login(ticket, captcha);
        Const.isLogin = true;

        String userInfo = HTHttpUtil.userInfo();
        JSONObject userInfoObject = JSON.parseObject(userInfo);
        if (userInfoObject.getInteger("code") == 200) {
            String data = userInfoObject.getString("data");
            JSONObject userObject = JSON.parseObject(data);
            Const.accountInfo.setReportName(userObject.getString("dwmc"));
            Const.accountInfo.setRegisterUser(userObject.getString("xm"));
            FileUtil.saveCache(JSON.toJSONString(Const.accountInfo));
        }
        return true;
    }

    @Override
    public void pullData(SyncDataCompleteListener listener) {
        if (Const.isPulling) {
            throw new BizException("正在拉取数据，不能重复操作");
        }
        Const.isPulling = true;
        String endDate = DateUtil.today();
        String startDate = DateUtil.formatDate(DateUtil.offsetDay(DateUtil.date(), -30));

        executorService.execute(() -> {
            kmMap.forEach((key, value) -> {
                typeMap.forEach((typeKey, typeValue) -> {
                    cMap.forEach((cKey, cValue) -> {
                        try {
                            System.out.println(key + "====" + typeKey + "=====" + cKey);
                            Map<String, Object> params = getQueryParams(value, cValue, typeValue, typeNameMap.get(typeValue), startDate, endDate);
                            System.out.println(JSON.toJSONString(params));
                            InputStream inputStream = HTHttpUtil.executeDown(params);

                            if (inputStream != null && inputStream.available() > 1) {
                                String filePath = FileUtil.saveFile(inputStream, endDate + Const.splitFlag + key + Const.splitFlag + typeKey + Const.splitFlag + cKey + ".xls");

                                BatchInfo batchInfo = new BatchInfo();
                                batchInfo.setGetTime(DateUtil.now());
                                batchInfo.setInfoType(typeKey);
                                batchInfo.set_abstract(endDate + Const.splitFlag + cKey + Const.splitFlag + key + Const.splitFlag + typeKey);
                                HTHttpUtil.uploadFile(filePath, batchInfo);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Const.isPulling = false;
                        }
                    });
                });
            });
            Const.isPulling = false;
            listener.onComplete();
        });
    }

    public Map<String, Object> getQueryParams(String kskm, String kscx, String type, String typeName, String startDate, String endDate) {
        Map<String, Object> map = new HashMap<>();
        map.put("ykrqstart", startDate);
        map.put("ykrqend", endDate);
        map.put("ksrqstart", startDate);
        map.put("ksrqend", endDate);
        map.put("type", type);    //  1=预约查询 "学员预约信息统计表"     2=考试成绩  "学员考试成绩统计表"
        map.put("typeName", typeName);
        map.put("kskm", kskm);    //考试科目  1 ,2 ,3 ,4
        map.put("kscx", kscx);   //考试车型  A1,A2,A3,B1,B2,C1,C2,C5
//        map.put("xm", "");       //姓名
        return map;
    }

    private static Map<String, String> getKmMap() {
        Map<String, String> kmMap = new HashMap<>();
        kmMap.put("科目1", "1");
        kmMap.put("科目2", "2");
        kmMap.put("科目3", "3");
        kmMap.put("科目4", "4");
        return kmMap;
    }

    private static Map<String, String> getTypeMap() {
        Map<String, String> typeMap = new HashMap<>();
        typeMap.put("约考", "1");
        typeMap.put("成绩", "2");
        return typeMap;
    }

    private static Map<String, String> getCMap() {
        Map<String, String> typeMap = new HashMap<>();
        typeMap.put("C1", "C1");
        typeMap.put("C2", "C2");
        return typeMap;
    }

    private static Map<String, String> getTypeNameMap() {
        Map<String, String> typeMap = new HashMap<>();
        typeMap.put("1", "学员预约信息统计表");
        typeMap.put("2", "学员考试成绩统计表");
        return typeMap;
    }
}
