package com.qiaomu.model;

import java.io.Serializable;

/**
 * @author: qiaomu
 * @date: 2021/7/27 14:12
 * @Description: TODO
 */
public class BatchInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    private String getTime;
    private String infoType;//类型，成绩、约考
    private String _abstract;
    private int rowsCount;

    public String getGetTime() {
        return getTime;
    }

    public void setGetTime(String getTime) {
        this.getTime = getTime;
    }

    public String getInfoType() {
        return infoType;
    }

    public void setInfoType(String infoType) {
        this.infoType = infoType;
    }

    public String get_abstract() {
        return _abstract;
    }

    public void set_abstract(String _abstract) {
        this._abstract = _abstract;
    }

    public int getRowsCount() {
        return rowsCount;
    }

    public void setRowsCount(int rowsCount) {
        this.rowsCount = rowsCount;
    }
}
