package com.qiaomu.model;

/**
 * @author: qiaomu
 * @date: 2021/7/15 10:31
 * @Description: TODO
 */
public class Result<T> {

    private int code;
    private T data;
    private String msg;



    public Result(int code, T data) {
        this.code = code;
        this.data = data;
    }

    public Result(int code, T data, String msg) {
        this.code = code;
        this.data = data;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static Result<String> fail() {
        return new Result<>(-1, null);
    }

    public static Result<String> fail(String msg) {
        return new Result<>(-1, null, msg);
    }

    public static <T> Result<T> success(T t) {
        return new Result<>(0, t, "success");
    }
}
