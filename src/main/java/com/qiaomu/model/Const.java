package com.qiaomu.model;

import java.util.concurrent.Executors;

/**
 * @author: qiaomu
 * @date: 2021/7/16 15:25
 * @Description: TODO
 */
public class Const {

    public static AccountInfo accountInfo;

//    public static String domain = "https://xj.122.gov.cn";

    public static String cookies = null;

    public static String localPath = System.getProperty("user.dir");

    public static String filePath = "";
    public static boolean isLogin = false;
    public static boolean isPulling = false;

    public static String splitFlag = "_";

    /** 下载存放地址 */
    public static String saveFilePath = "C:\\driver";


}
