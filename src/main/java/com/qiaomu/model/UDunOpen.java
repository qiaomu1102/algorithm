package com.qiaomu.model;

/**
 * @author: qiaomu
 * @date: 2021/7/15 11:25
 * @Description: TODO
 */
public class UDunOpen {

    /**
     * errorCode : 0
     * function : IA300Open
     * rtn : 0
     */

    private int errorCode;
    private String function;
    private int rtn;

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public int getRtn() {
        return rtn;
    }

    public void setRtn(int rtn) {
        this.rtn = rtn;
    }
}
