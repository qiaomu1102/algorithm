package com.qiaomu.model;

/**
 * @author: qiaomu
 * @date: 2021/7/16 11:52
 * @Description: TODO
 */
public class SeedResult {

    /**
     * digest : b3b4c5575989d57cfc0f229b6277a93fafacacf3
     * errorCode : 0
     * function : IA300SHA1WithSeed
     * rtn : 0
     */

    private String digest;
    private int errorCode;
    private String function;
    private int rtn;

    public String getDigest() {
        return digest;
    }

    public void setDigest(String digest) {
        this.digest = digest;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public int getRtn() {
        return rtn;
    }

    public void setRtn(int rtn) {
        this.rtn = rtn;
    }
}
