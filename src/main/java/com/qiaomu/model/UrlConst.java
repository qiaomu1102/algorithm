package com.qiaomu.model;

import java.net.HttpCookie;
import java.util.List;

/**
 * @author: qiaomu
 * @date: 2021/7/15 18:42
 * @Description: TODO
 */
public interface UrlConst {


    /** 登陆页面 */
    String loginPage = "/m/login?t=2";

    String checkType = "/m/tmri/captcha/mcheckType?checktype=sgcFtVUtwJZtzwTT";

    /** 获取图形验证码图片 */
    String captcha = "/m/tmri/captcha/math?nocache=";

    /** 图片识别 */
    String ocr_captcha_domain = "http://106.75.148.97:19996/ocr";

    /** 图片识别的参数 */
    String ocr_captcha_params = "webSide=https://d.122.gov.cn&b64=IMGBASE64STR&sign=htbb";

    /** 检查key */
    String check_key = "/user/m/login/checkkey";

    /** 登陆key */
    String login_key = "/user/m/loginkey";

    /** 用户信息 */
    String userInfo = "/user/m/userinfo/basic";

    /** 考试成绩下载 */
    String execute_down = "/drv/yycx/train/executedown";

    /** 上传数据到标准接口 */
//    String uploadUrl = "https://testinterface.haitunbx.com/htbx-interface/udun122/upload";
    String uploadUrl = "https://interface.haitunbx.com/htbx-interface/udun122/upload";

    /** 下载 */
    String downLoadUrl = "http://nfs.hthu.com.cn/file/sys_file/driverClient.exe";

}
