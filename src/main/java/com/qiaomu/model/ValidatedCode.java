package com.qiaomu.model;

/**
 * @author: qiaomu
 * @date: 2021/7/16 11:43
 * @Description: TODO
 */
public class ValidatedCode {


    /**
     * code : 200
     * message : good
     * result : 6
     */

    private int code;
    private String message;
    private String result;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
