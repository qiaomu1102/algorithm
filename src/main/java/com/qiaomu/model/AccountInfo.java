package com.qiaomu.model;

import java.io.Serializable;

/**
 * @author: qiaomu
 * @date: 2021/7/15 10:08
 * @Description: TODO
 */
public class AccountInfo implements Serializable {

    public static final long serialVersionUID = 1;
    private String reportName;
    private String account;//登陆账号
    private String authKey;//签名
    private String webSite;//省网地址
    private String uKeyId;//U盾Key
    private String organName; //驾校名称
    private String registerOrgan;       //驾校名称
    private String registerUser;
    private String updateTime = "";

    private String passWord;     //密码

    public String getRegisterUser() {
        return registerUser;
    }

    public void setRegisterUser(String registerUser) {
        this.registerUser = registerUser;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getAuthKey() {
        return authKey;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    public String getWebSite() {
        return webSite;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    public String getuKeyId() {
        return uKeyId;
    }

    public void setuKeyId(String uKeyId) {
        this.uKeyId = uKeyId;
    }

    public String getOrganName() {
        return organName;
    }

    public void setOrganName(String organName) {
        this.organName = organName;
    }

    public String getRegisterOrgan() {
        return registerOrgan;
    }

    public void setRegisterOrgan(String registerOrgan) {
        this.registerOrgan = registerOrgan;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }
}
