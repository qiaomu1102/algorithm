package com.qiaomu.model;

/**
 * @author: qiaomu
 * @date: 2021/7/15 11:25
 * @Description: TODO
 */
public class UDunID {


    /**
     * HardwareID : dc59550653ba91642dbda7bc54d64ca5
     * errorCode : 0
     * function : IA300GetUID
     * rtn : 0
     */

    private String HardwareID;
    private int errorCode;
    private String function;
    private int rtn;

    public String getHardwareID() {
        return HardwareID;
    }

    public void setHardwareID(String HardwareID) {
        this.HardwareID = HardwareID;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public int getRtn() {
        return rtn;
    }

    public void setRtn(int rtn) {
        this.rtn = rtn;
    }
}
